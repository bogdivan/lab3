//Bogdan Ivan
//Student ID: 2032824
import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;
import LinearAlgebra.Vector3d;

public class Vector3dTests {
    Vector3d vec = new Vector3d(5.0,6.0,7.0);
    Vector3d vec2 = new Vector3d(1.0, 2.0, 3.0);

    @Test
    public void testGets(){
        assertEquals(5.0, vec.getX());
        assertEquals(6.0, vec.getY());
        assertEquals(7.0, vec.getZ());
    }

    @Test
    public void testMagnitude(){
        assertEquals(10.49, vec.magnitude(), 0.01);
    }

    @Test
    public void testDotProduct(){
        assertEquals(38, vec.dotProduct(vec2));
    }

    @Test
    public void testAdd(){
        Vector3d result = vec.add(vec2);
        assertEquals(6, result.getX());
    }
}
