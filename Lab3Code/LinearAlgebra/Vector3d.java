//Bogdan Ivan
//Student ID: 2032824
package LinearAlgebra;

public class Vector3d {
    private double x;
    private double y;
    private double z;

    public Vector3d(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double getX() {
        return this.x;
    }

    public double getY() {
        return this.y;
    }

    public double getZ() {
        return this.z;
    }

    public double magnitude(){
        return Math.sqrt(this.x * this.x + this.y * this.y + this.z * this.z);
    }

    public double dotProduct(Vector3d vec){
        return (this.x * vec.x + this.y * vec.y + this.z * vec.z);
    }

    public Vector3d add(Vector3d vec){
        return new Vector3d(this.x + vec.x, this.y + vec.y, this.z + vec.z);
    }
    
}
